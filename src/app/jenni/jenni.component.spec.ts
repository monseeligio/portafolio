import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JenniComponent } from './jenni.component';

describe('JenniComponent', () => {
  let component: JenniComponent;
  let fixture: ComponentFixture<JenniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JenniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JenniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
