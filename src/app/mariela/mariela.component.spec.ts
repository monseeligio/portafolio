import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarielaComponent } from './mariela.component';

describe('MarielaComponent', () => {
  let component: MarielaComponent;
  let fixture: ComponentFixture<MarielaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarielaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarielaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
