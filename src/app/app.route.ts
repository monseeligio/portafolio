import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { MonseComponent } from './monse/monse.component';
import { MarielaComponent } from './mariela/mariela.component';
import { JenniComponent } from './jenni/jenni.component';
import { FernandoComponent } from './fernando/fernando.component';
import { ChristoComponent } from './christo/christo.component';
import { HomeComponent } from './home/home.component';


export const ROUTES: Routes = [
  {path:'home',component:HomeComponent},//path: 'nombre de la ruta, ruta del componente'
  {path:'monse' , component:MonseComponent},
  {path:'mariela' , component:MarielaComponent},
  {path:'jenni' , component:JenniComponent},
  {path:'fernando' , component:FernandoComponent},
  {path:'christo' , component:ChristoComponent},
  {path: '', pathMatch:'full' ,redirectTo:'home'},
  //{path:'**', pathMatch:'full',redirectTo:'home'}
];
