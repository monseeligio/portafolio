import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MonseComponent } from './monse/monse.component';
import { MarielaComponent } from './mariela/mariela.component';
import { JenniComponent } from './jenni/jenni.component';
import { FernandoComponent } from './fernando/fernando.component';
import { ChristoComponent } from './christo/christo.component';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import {ROUTES} from './app.route'
import {MatStepperModule } from '@angular/material/stepper';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UnidadesComponent } from './unidades/unidades.component';

@NgModule({
  declarations: [
    AppComponent,
    MonseComponent,
    MarielaComponent,
    JenniComponent,
    FernandoComponent,
    ChristoComponent,
    HomeComponent,
    UnidadesComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES, {useHash:true}),
    MatStepperModule,
    BrowserAnimationsModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
