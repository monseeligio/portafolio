import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChristoComponent } from './christo.component';

describe('ChristoComponent', () => {
  let component: ChristoComponent;
  let fixture: ComponentFixture<ChristoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChristoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChristoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
